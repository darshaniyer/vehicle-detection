import numpy as np
import cv2
import pickle
import glob
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.patches as patches
from collections import deque

class Fit_info():
    def __init__(self):
        # was the line detected in the last iteration?
        self.detected = False 
        # fit coefficients
        self.coeff = deque(maxlen=31)
        # average coefficients
        self.coeff_avg = np.array([0,0,0], dtype='float')
        #difference in fit coefficients between last and new fits
        self.diffs = np.array([0,0,0], dtype='float') 
        # radius of curvature in km
        self.curvature = deque(maxlen=31)
        # vehicle position in km
        self.vehicle_position = deque(maxlen=31)  
        
def calc_obj_image_points(image_dir, nx, ny):
    """
    Function 'calc_obj_image_points' takes an image directory containing 
    checker board calibration images, number of corners in columns (nx) 
    and rows (ny), and return object points and image points
    """
    
    objp = np.zeros((ny*nx,3), np.float32) # shape 54x3
    objp[:,:2] = np.mgrid[0:nx,0:ny].T.reshape(-1,2) # shape 54x2

    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d points in real world space
    imgpoints = [] # 2d points in image plane.

    # # Make a list of calibration images
    images = glob.glob(image_dir)

    # Step through the list and search for chessboard corners
    for fname in images:
        img = mpimg.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)

        # Find the chessboard corners
        ret, corners = cv2.findChessboardCorners(gray, (9,6), None)

        # If found, add object points, image points
        if ret == True:
            objpoints.append(objp)
            imgpoints.append(corners)
    
    return objpoints, imgpoints

def calc_undistort(img, objpoints, imgpoints):
    """
    Function 'calc_undistort' takes an image, object points, and image points,
    performs the camera calibration, image distortion correction, and
    returns the undistorted image    
    """
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
    undist = cv2.undistort(img, mtx, dist, None, mtx)
    
    return undist, mtx, dist

def corners_unwarp(undistorted, mtx, dist, src, dst):   
    """
    Function 'corners_unwarp' takes an image, object points, and image points
    performs the camera calibration, image distortion correction, and
    returns the undistorted image
    """
    # Grab the image shape
    img_size = (undistorted.shape[1], undistorted.shape[0])    
    # Given src and dst points, calculate the perspective transform matrix
    M = cv2.getPerspectiveTransform(src, dst)
    # Warp the image using OpenCV warpPerspective()
    warped = cv2.warpPerspective(undistorted, M, img_size)      
    # Given dst and src points, calculate the inverse perspective transform matrix
    Minv = cv2.getPerspectiveTransform(dst, src)
    
    return warped, M, Minv

def gaussian_blur(img, kernel_size):
    """Applies a Gaussian Noise kernel"""
    return cv2.GaussianBlur(img, (kernel_size, kernel_size), 0)

def region_of_interest(img):
    """
    Applies an image mask.
    
    Only keeps the region of the image defined by the polygon
    formed from `vertices`. The rest of the image is set to black.
    """
    imshape = img.shape
    tbs_x = 0.1 # polygon_bottom_start for x (column)
    tts_x = 0.4 # polygon_top_start for x (column)
    tte_x = 0.6 # polygon_top_end for x (column)
    tbe_x = 0.9 # polygon_bottom_end for x (column)
    tbs_y = 1   # polygon_bottom_start for y (row)
    tts_y = 0.6 # polygon_top_start for y (row) # changed from 0.55
    tte_y = 0.6 # polygon_top_end for y (row) # changed from 0.55
    tbe_y = 1   # polygon_bottom_end for y (row)
    vertices = np.array([[(tbs_x*imshape[1],tbs_y*imshape[0]),(tts_x*imshape[1],tts_y*imshape[0]), 
                          (tte_x*imshape[1],tte_y*imshape[0]), (tbe_x*imshape[1],tbe_y*imshape[0])]], dtype=np.int32)

    #defining a blank mask to start with
    mask = np.zeros_like(img)   
    
    #defining a 3 channel or 1 channel color to fill the mask with depending on the input image
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255
        
    #filling pixels inside the polygon defined by "vertices" with the fill color    
    cv2.fillPoly(mask, vertices, ignore_mask_color)
    
    #returning the image only where mask pixels are nonzero
    masked_image = cv2.bitwise_and(img, mask)
    
    return masked_image

def region_of_interest2(img):
    """
    Applies an image mask.
    
    Only keeps the region of the image defined by the polygon
    formed from `vertices`. The rest of the image is set to black.
    """
    imshape = img.shape
    tbs_x = 0 # polygon_bottom_start for x (column)
    tts_x = 0 # polygon_top_start for x (column)
    tte_x = 1 # polygon_top_end for x (column)
    tbe_x = 1 # polygon_bottom_end for x (column)
    tbs_y = 1   # polygon_bottom_start for y (row)
    tts_y = 0.6 # polygon_top_start for y (row) # changed from 0.55
    tte_y = 0.6 # polygon_top_end for y (row) # changed from 0.55
    tbe_y = 1   # polygon_bottom_end for y (row)
    vertices = np.array([[(tbs_x*imshape[1],tbs_y*imshape[0]),(tts_x*imshape[1],tts_y*imshape[0]), 
                          (tte_x*imshape[1],tte_y*imshape[0]), (tbe_x*imshape[1],tbe_y*imshape[0])]], dtype=np.int32)

    #defining a blank mask to start with
    mask = np.zeros_like(img)   
    
    #defining a 3 channel or 1 channel color to fill the mask with depending on the input image
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255
        
    #filling pixels inside the polygon defined by "vertices" with the fill color    
    cv2.fillPoly(mask, vertices, ignore_mask_color)
    
    #returning the image only where mask pixels are nonzero
    masked_image = cv2.bitwise_and(img, mask)
    
    return masked_image


def abs_sobel_thresh(img, orient='x', sobel_kernel=3, thresh=(0,255)):
    """
    Function 'abs_sobel_thresh' applies Sobel x or y,
    then takes an absolute value and applies a threshold.    
    """
    # Apply the following steps to img
    # 1) Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # 2) Take the derivative in x or y given orient = 'x' or 'y'
    if (orient == 'x'):
        sobel = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
    else:
        sobel = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel)
    # 3) Take the absolute value of the derivative or gradient
    sobel_abs = np.absolute(sobel)
    # 4) Scale to 8-bit (0 - 255) then convert to type = np.uint8
    sobel_abs_scaled = np.uint8(255 * sobel_abs / np.max(sobel_abs))
    # 5) Create a mask of 1's where the scaled gradient magnitude
    grad_binary = np.zeros_like(sobel_abs_scaled)
    grad_binary[(sobel_abs_scaled >= thresh[0]) & (sobel_abs_scaled <= thresh[1])] = 1
    # is > thresh_min and < thresh_max
    
    return grad_binary

def mag_thresh(img, sobel_kernel=3, mag_thresh=(0, 255)):
    """
    Function 'mag_thresh' applies Sobel x and y,
    then computes the magnitude of the gradient
    and applies a threshold
    """
    # Apply the following steps to img
    # 1) Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # 2) Take the gradient in x and y separately
    sobel_x = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
    sobel_y = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel)
    # 3) Calculate the magnitude
    sobel_m = np.sqrt(sobel_x ** 2 + sobel_y ** 2)
    # 4) Scale to 8-bit (0 - 255) and convert to type = np.uint8
    sobel_m_scaled = np.uint8(255 * sobel_m / np.max(sobel_m))
    # 5) Create a binary mask where mag thresholds are met
    mag_binary = np.zeros_like(sobel_m_scaled)
    mag_binary[(sobel_m_scaled >= mag_thresh[0]) & (sobel_m_scaled <= mag_thresh[1])] = 1
       
    return mag_binary

def dir_threshold(img, sobel_kernel=3, thresh=(0, np.pi/2)):
    """
    Function 'dir_threshold' applies Sobel x and y,
    then computes the direction of the gradient
    and applies a threshold.
    """
    # Apply the following steps to img
    # 1) Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # 2) Take the gradient in x and y separately
    sobel_x = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
    sobel_y = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel)
    # 3) Take the absolute value of the x and y gradients
    abs_sobelx = np.absolute(sobel_x)
    abs_sobely = np.absolute(sobel_y)
    # 4) Use np.arctan2(abs_sobely, abs_sobelx) to calculate the direction of the gradient
    sobel_o = np.arctan2(abs_sobely, abs_sobelx)
    # 5) Create a binary mask where direction thresholds are met
    dir_binary = np.zeros_like(sobel_o)    
    dir_binary[(sobel_o >= thresh[0]) & (sobel_o <= thresh[1])] = 1    
    
    return dir_binary

def hls_select_thresh(img, channel_sel = 's', thresh=(0, 255)):    
    """
    Function 'hls_select_thresh' applies color thresholding in HLS space
    """
    hls = cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
    h_channel = hls[:,:,0]
    l_channel = hls[:,:,1]
    s_channel = hls[:,:,2]
    
#     h_channel = np.uint8(255*(hls[:,:,0]/np.max(hls[:,:,0])))
#     l_channel = np.uint8(255*(hls[:,:,1]/np.max(hls[:,:,1])))
#     s_channel = np.uint8(255*(hls[:,:,2]/np.max(hls[:,:,2])))
    
    hls_binary = np.zeros_like(h_channel)
    if channel_sel == 'h':
        hls_binary[(h_channel >= thresh[0]) & (h_channel <= thresh[1])] = 1
    elif channel_sel == 'l':
        hls_binary[(l_channel >= thresh[0]) & (l_channel <= thresh[1])] = 1
    elif channel_sel == 's':    
        hls_binary[(s_channel >= thresh[0]) & (s_channel <= thresh[1])] = 1
        
    return hls_binary

def hsv_select_thresh(img, channel_sel = 's', thresh=(0, 255)):   
    """
    Function 'hsv_select_thresh' applies color thresholding in HSV space
    """
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    h_channel = hsv[:,:,0]
    s_channel = hsv[:,:,1]
    v_channel = hsv[:,:,2]
    
    hsv_binary = np.zeros_like(h_channel)
    if channel_sel == 'h':
        hsv_binary[(h_channel >= thresh[0]) & (h_channel <= thresh[1])] = 1
    elif channel_sel == 's':    
        hsv_binary[(s_channel >= thresh[0]) & (s_channel <= thresh[1])] = 1
    elif channel_sel == 'v':
        hsv_binary[(v_channel >= thresh[0]) & (v_channel <= thresh[1])] = 1
        
    return hsv_binary

def yuv_select_thresh(img, channel_sel = 'y', thresh=(0, 255)):   
    """
    Function 'hsv_select_thresh' applies color thresholding in HSV space
    """
    yuv = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
    y_channel = yuv[:,:,0]
    u_channel = yuv[:,:,1]
    v_channel = yuv[:,:,2]
    
    yuv_binary = np.zeros_like(y_channel)
    if channel_sel == 'y':
        yuv_binary[(y_channel >= thresh[0]) & (y_channel <= thresh[1])] = 1
    elif channel_sel == 'u':    
        yuv_binary[(u_channel >= thresh[0]) & (u_channel <= thresh[1])] = 1
    elif channel_sel == 'v':
        yuv_binary[(v_channel >= thresh[0]) & (v_channel <= thresh[1])] = 1
        
    return yuv_binary

def luv_select_thresh(img, channel_sel = 'l', thresh=(0, 255)):   
    """
    Function 'hsv_select_thresh' applies color thresholding in HSV space
    """
    luv = cv2.cvtColor(img, cv2.COLOR_RGB2LUV)
    l_channel = luv[:,:,0]
    u_channel = luv[:,:,1]
    v_channel = luv[:,:,2]
    
    luv_binary = np.zeros_like(l_channel)
    if channel_sel == 'l':
        luv_binary[(l_channel >= thresh[0]) & (l_channel <= thresh[1])] = 1
    elif channel_sel == 'u':    
        luv_binary[(u_channel >= thresh[0]) & (u_channel <= thresh[1])] = 1
    elif channel_sel == 'v':
        luv_binary[(v_channel >= thresh[0]) & (v_channel <= thresh[1])] = 1
        
    return luv_binary

def ycrcb_select_thresh(img, channel_sel = 'y', thresh=(0, 255)):   
    """
    Function 'hsv_select_thresh' applies color thresholding in HSV space
    """
    ycrcb = cv2.cvtColor(img, cv2.COLOR_RGB2YCrCb)
    y_channel = ycrcb[:,:,0]
    cr_channel = ycrcb[:,:,1]
    cb_channel = ycrcb[:,:,2]
    
    ycrcb_binary = np.zeros_like(y_channel)
    if channel_sel == 'y':
        ycrcb_binary[(y_channel >= thresh[0]) & (y_channel <= thresh[1])] = 1
    elif channel_sel == 'cr':    
        ycrcb_binary[(cr_channel >= thresh[0]) & (cr_channel <= thresh[1])] = 1
    elif channel_sel == 'cb':
        ycrcb_binary[(cb_channel >= thresh[0]) & (cb_channel <= thresh[1])] = 1
        
    return ycrcb_binary

def rgb_select_thresh(img, channel_sel = 'r', thresh=(0, 255)):  
    """
    Function 'rgb_select_thresh' applies color thresholding in RGB space
    """
    r_channel = img[:,:,0]
    g_channel = img[:,:,1]
    b_channel = img[:,:,2]    

#     r_channel = np.uint8(255*(img[:,:,0]/np.max(img[:,:,0])))
#     g_channel = np.uint8(255*(img[:,:,1]/np.max(img[:,:,1])))
#     b_channel = np.uint8(255*(img[:,:,2]/np.max(img[:,:,2])))
    
    rgb_binary = np.zeros_like(r_channel)
    if channel_sel == 'r':
        rgb_binary[(r_channel >= thresh[0]) & (r_channel <= thresh[1])] = 1
    elif channel_sel == 'g':    
        rgb_binary[(g_channel >= thresh[0]) & (g_channel <= thresh[1])] = 1
    elif channel_sel == 'b':
        rgb_binary[(b_channel >= thresh[0]) & (b_channel <= thresh[1])] = 1
        
    return rgb_binary

def lab_select_thresh(img, channel_sel = 'b', thresh=(0, 255)):   
    """
    Function 'hsv_select_thresh' applies color thresholding in HSV space
    """
    lab = cv2.cvtColor(img, cv2.COLOR_RGB2LAB)
    l_channel = lab[:,:,0]
    a_channel = lab[:,:,1]
    b_channel = lab[:,:,2]
    
#     l_channel = np.uint8(255*(lab[:,:,0]/np.max(lab[:,:,0])))
#     a_channel = np.uint8(255*(lab[:,:,1]/np.max(lab[:,:,1])))
#     b_channel = np.uint8(255*(lab[:,:,2]/np.max(lab[:,:,2])))
    
    lab_binary = np.zeros_like(l_channel)
    if channel_sel == 'l':
        lab_binary[(l_channel >= thresh[0]) & (l_channel <= thresh[1])] = 1
    elif channel_sel == 'a':    
        lab_binary[(a_channel >= thresh[0]) & (a_channel <= thresh[1])] = 1
    elif channel_sel == 'b':
        lab_binary[(b_channel >= thresh[0]) & (b_channel <= thresh[1])] = 1
        
    return lab_binary

def lf_histogram(binary_warped):
    """
    Function lf_histogram takes in bird's eye view perspective-transformed image,
    applies histogram-based method in vertical window steps to find prominent peaks
    corresponding to left and right lanes, finds pixel indices corresponding to each,
    and finally applies 2nd-order polynomial fit. Returns pixel indices and 2nd-order
    polynomial fits.
    """
    
    # Take a histogram of the bottom half of the image
    histogram = np.sum(binary_warped[binary_warped.shape[0]//2:,:], axis=0)
    # Create an output image to draw on and  visualize the result
    out_img = np.dstack((binary_warped, binary_warped, binary_warped))*255    
    # Find the peak of the left and right halves of the histogram
    # These will be the starting point for the left and right lines
    midpoint = np.int(histogram.shape[0]//2)
    leftx_base = np.argmax(histogram[:midpoint])
    rightx_base = np.argmax(histogram[midpoint:]) + midpoint
    
    # Choose the number of sliding windows
    nwindows = 9
    # Set height of windows
    window_height = np.int(binary_warped.shape[0]//nwindows)    
    # Identify the x and y positions of all nonzero pixels in the image
    nonzero = binary_warped.nonzero()   
    nonzeroy = np.array(nonzero[0])   
    nonzerox = np.array(nonzero[1])    
    # Current positions to be updated for each window
    leftx_current = leftx_base
    rightx_current = rightx_base
    # Set the width of the windows +/- margin
    margin = 100
    # Set minimum number of pixels found to recenter window
    minpix = 50
    
    left_lane_inds = []
    right_lane_inds = []
    # Step through the windows one by one
    for window in range(nwindows):
        # Identify window boundaries in x and y (and right and left)
        win_y_low = binary_warped.shape[0] - (window+1)*window_height
        win_y_high = binary_warped.shape[0] - window*window_height
        win_xleft_low = leftx_current - margin
        win_xleft_high = leftx_current + margin
        win_xright_low = rightx_current - margin
        win_xright_high = rightx_current + margin
        # Draw the windows on the visualization image
        cv2.rectangle(out_img,(win_xleft_low,win_y_low),(win_xleft_high,win_y_high),
        (0,255,0), 2) 
        cv2.rectangle(out_img,(win_xright_low,win_y_low),(win_xright_high,win_y_high),
        (0,255,0), 2) 
        # Identify the nonzero pixels in x and y within the window
        good_left_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & 
        (nonzerox >= win_xleft_low) &  (nonzerox < win_xleft_high)).nonzero()[0]
        good_right_inds = ((nonzeroy >= win_y_low) & (nonzeroy < win_y_high) & 
        (nonzerox >= win_xright_low) &  (nonzerox < win_xright_high)).nonzero()[0]
        # Append these indices to the lists
        left_lane_inds.append(good_left_inds)
        right_lane_inds.append(good_right_inds)
        # If you found > minpix pixels, recenter next window on their mean position
        if len(good_left_inds) > minpix:
            leftx_current = np.int(np.mean(nonzerox[good_left_inds]))
        if len(good_right_inds) > minpix:        
            rightx_current = np.int(np.mean(nonzerox[good_right_inds]))

    # Concatenate the arrays of indices
    left_lane_inds = np.concatenate(left_lane_inds)
    right_lane_inds = np.concatenate(right_lane_inds)        

    # Extract left and right line pixel positions
    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds] 
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds] 

    # Fit a second order polynomial to each
    left_fit = []
    right_fit = []
    fit_found = False
    if (len(lefty) > 0 and len(leftx) > 0 and len(righty) > 0 and len(rightx) > 0):
        fit_found = True
        left_fit = np.polyfit(lefty, leftx, 2)
        right_fit = np.polyfit(righty, rightx, 2)
        
    return fit_found, np.array(left_fit), np.array(right_fit), np.array(left_lane_inds), np.array(right_lane_inds), out_img
    
def apply_line_fit(binary_warped, left_fit, right_fit):
    """
    Function apply_line_fit applies previously calculated left and right lane fits 
    on an input input image and finds the new left and right lane pixels    
    """    
    nonzero = binary_warped.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])

    margin = 100
    left_lane_inds = ((nonzerox > (left_fit[0]*(nonzeroy**2) + left_fit[1]*nonzeroy + 
    left_fit[2] - margin)) & (nonzerox < (left_fit[0]*(nonzeroy**2) + 
    left_fit[1]*nonzeroy + left_fit[2] + margin))) 

    right_lane_inds = ((nonzerox > (right_fit[0]*(nonzeroy**2) + right_fit[1]*nonzeroy + 
    right_fit[2] - margin)) & (nonzerox < (right_fit[0]*(nonzeroy**2) + 
    right_fit[1]*nonzeroy + right_fit[2] + margin)))  
    
    # Extract left and right line pixel positions
    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds] 
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds] 
    
    # Fit a second order polynomial to each
    left_fit = []
    right_fit = []
    fit_found = False
    if (len(lefty) > 0 and len(leftx) > 0 and len(righty) > 0 and len(rightx) > 0):
        fit_found = True
        left_fit = np.polyfit(lefty, leftx, 2)
        right_fit = np.polyfit(righty, rightx, 2)
        
    return fit_found, np.array(left_fit), np.array(right_fit), np.array(left_lane_inds), np.array(right_lane_inds)
        
def visualize_lane_fit(binary_warped, left_fit, right_fit, left_lane_inds, right_lane_inds, out_img):
    
    nonzero = binary_warped.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])
    
    # Generate x and y values for plotting
    ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0])

    left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
    right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

    out_img[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
    out_img[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [0, 0, 255]  
    
#     plt.figure()
#     plt.imshow(out_img)
#     plt.plot(left_fitx, ploty, color='yellow')
#     plt.plot(right_fitx, ploty, color='yellow')
#     plt.xlim(0, 1280)
#     plt.ylim(720, 0)
    
    return out_img, left_fitx, right_fitx, ploty       

def visualize_lanes(binary_warped, left_fit, right_fit, left_lane_inds, right_lane_inds):
    """
    Function visualize_lanes visualizes left and right lane pixels and the corresponding
    lane fit lines passing through them. It also draws a thick region around each fit.
    """    
    nonzero = binary_warped.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])
    
    # Generate x and y values for plotting
    ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0])

    left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
    right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]
    
    # Create an image to draw on and an image to show the selection window
    out_img = np.dstack((binary_warped, binary_warped, binary_warped))*255
    window_img = np.zeros_like(out_img)
    # Color in left and right line pixels
    out_img[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
    out_img[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [0, 0, 255]

    # Generate a polygon to illustrate the search window area
    # And recast the x and y points into usable format for cv2.fillPoly()
    margin = 100
    left_line_window1 = np.array([np.transpose(np.vstack([left_fitx-margin, ploty]))])

    left_line_window2 = np.array([np.flipud(np.transpose(np.vstack([left_fitx+margin, 
                                  ploty])))])

    left_line_pts = np.hstack((left_line_window1, left_line_window2))

    right_line_window1 = np.array([np.transpose(np.vstack([right_fitx-margin, ploty]))])
    right_line_window2 = np.array([np.flipud(np.transpose(np.vstack([right_fitx+margin, 
                                  ploty])))])
    right_line_pts = np.hstack((right_line_window1, right_line_window2))
    
    # Draw the lane onto the warped blank image
    cv2.fillPoly(window_img, np.int_([left_line_pts]), (0,255, 0))
    cv2.fillPoly(window_img, np.int_([right_line_pts]), (0,255, 0))
    result = cv2.addWeighted(out_img, 1, window_img, 0.3, 0)
#     plt.imshow(result)
#     plt.plot(left_fitx, ploty, color='yellow')
#     plt.plot(right_fitx, ploty, color='yellow')
#     plt.xlim(0, 1280)
#     plt.ylim(720, 0)
    
    return window_img, result, left_fitx, right_fitx, ploty

def visualize_lanes_real_roads(undistorted, binary_warped, Minv, left_fit, right_fit):
    """
    Function visualize_lanes_real_roads plots a drivable region in front on an 
    undistorted image. The drivable region is calculated based on inverse perspective
    transform matrix as well as left and right lane fits
    """  
    left_curverad_km = None
    right_curverad_km = None 
    offset_m = None
    poly_corners = []
    
    if ( (len(left_fit) > 0) and (len(right_fit) > 0) ):  
        ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0])    

        left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
        right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]

        # Create an image to draw the lines on
        warp_zero = np.zeros_like(binary_warped).astype(np.uint8)
        color_warp1 = np.dstack((warp_zero, warp_zero, warp_zero))
        color_warp2 = np.dstack((warp_zero, warp_zero, warp_zero))

        # Recast the x and y points into usable format for cv2.fillPoly()
        pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
        pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
        pts = np.hstack((pts_left, pts_right))

        # Draw the lane onto the warped blank image
        cv2.polylines(color_warp1, np.int_([pts_left]), True, (255, 0, 0))
        cv2.polylines(color_warp1, np.int_([pts_right]), True, (255, 0, 0))
        cv2.fillPoly(color_warp2, np.int_([pts]), (0,255, 0))

        # Warp the blank back to original image space using inverse perspective matrix (Minv)
        newwarp1 = cv2.warpPerspective(color_warp1, Minv, (undistorted.shape[1], undistorted.shape[0])) 
        newwarp2 = cv2.warpPerspective(color_warp2, Minv, (undistorted.shape[1], undistorted.shape[0])) 
        # Combine the result with the original image
        processed_image = cv2.addWeighted(undistorted, 1, newwarp1, 0.7, 0)    
        processed_image = cv2.addWeighted(processed_image, 1, newwarp2, 0.3, 0)  

        # Calculate curvature
        left_curverad_km, right_curverad_km, offset_m = \
            calc_curvature_and_vehicle_position(binary_warped, left_fit, right_fit)
            
        l_curv_text = "L. Lane Curvature: %5.2f km " % left_curverad_km
        r_curv_text = "R. Lane Curvature: %5.2f km " % right_curverad_km
        if (offset_m >= 0):        
            position_text = "C. Position is %2.2f m left of center" % abs(offset_m) 
        else:
            position_text = "C. Position is %2.2f m right of center" % abs(offset_m)            
 
        cv2.putText(processed_image, l_curv_text, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 4, cv2.LINE_AA)
        cv2.putText(processed_image, r_curv_text, (50, 110), cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 4, cv2.LINE_AA)
        cv2.putText(processed_image, position_text, (50, 170), cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 4, cv2.LINE_AA)
        
#         plt.figure()
#         plt.imshow(processed_image)        
#    else:
#         plt.figure()
#         processed_image = undistorted.copy()
#         plt.imshow(processed_image)   

        inds = cv2.findNonZero(newwarp2[:,:,1])
        a = np.array(inds).squeeze()
        poly_corners = np.float32([[a[0][0], a[0][1]], [np.min(a[:,0]), np.max(a[:,1])], 
                                  [a[-1][0], a[-1][1]], [np.max(a[:,1]), np.min(a[:,1])]])
        
    return processed_image, left_curverad_km, right_curverad_km, offset_m, poly_corners
    
def calc_curvature_and_vehicle_position(binary_warped, left_fit, right_fit):
    """
    Function calc_curvature calculates curvature of the lanes on 
    the real roads
    """     
    ploty = np.linspace(0, binary_warped.shape[0]-1, binary_warped.shape[0]) 

    left_fitx = left_fit[0]*ploty**2 + left_fit[1]*ploty + left_fit[2]
    right_fitx = right_fit[0]*ploty**2 + right_fit[1]*ploty + right_fit[2]
    
    y_eval = np.max(ploty)
    if (np.absolute(2*left_fit[0]) == 0):
        left_curverad = 0.0
    else:
        left_curverad = ((1 + (2*left_fit[0]*y_eval + left_fit[1])**2)**1.5) / np.absolute(2*left_fit[0])
        
    if (np.absolute(2*right_fit[0]) == 0):
        right_curverad = 0.0
    else:    
        right_curverad = ((1 + (2*right_fit[0]*y_eval + right_fit[1])**2)**1.5) / np.absolute(2*right_fit[0])    
    
    # In real world space units

    # Define conversions in x and y from pixels space to meters
    ym_per_pix = 30/720 # meters per pixel in y dimension
    xm_per_pix = 3.7/700 # meters per pixel in x dimension # one lane

    # Fit new polynomials to x,y in world space
    left_fit_cr = np.polyfit(ploty*ym_per_pix, left_fitx*xm_per_pix, 2)
    right_fit_cr = np.polyfit(ploty*ym_per_pix, right_fitx*xm_per_pix, 2)
    # Calculate the new radii of curvature in meters
    
    if (np.absolute(2*left_fit_cr[0]) == 0):
        left_curverad_m = 0.0
    else:
        left_curverad_m = ((1 + (2*left_fit_cr[0]*y_eval*ym_per_pix + left_fit_cr[1])**2)**1.5) / np.absolute(2*left_fit_cr[0])
    
    if (np.absolute(2*right_fit_cr[0]) == 0):
        right_curverad_m = 0.0
    else:
        right_curverad_m = ((1 + (2*right_fit_cr[0]*y_eval*ym_per_pix + right_fit_cr[1])**2)**1.5) / np.absolute(2*right_fit_cr[0])
    
    # vehicle position with respect to the lane center
    image_center = binary_warped.shape[1]/2 # center of x-axis
    lane_center = (left_fitx[-1] + right_fitx[-1])/2 # -1 refers to the bottom of the image where the ego car is located
    offset_pixel_space = image_center-lane_center
    offset_m = offset_pixel_space*xm_per_pix

    return left_curverad_m/1000.0, right_curverad_m/1000.0, offset_m

def robust_average(data, norm_order):
        
    data_median=np.median(data, axis=0) 
    
    epsilon=1
    dist = [float(np.linalg.norm(x-data_median,ord=norm_order)+epsilon) for x in data]
    
    weights = np.divide(1.0, dist)
    
    weights = np.divide(weights, np.linalg.norm(weights,ord=1))
    
    centroid = sum(w*x for (w,x) in zip(weights, data))  
    
    return (centroid)     

##### Convolution-based method
def window_mask(width, height, img_ref, center, level):
    output = np.zeros_like(img_ref)
    output[int(img_ref.shape[0]-(level+1)*height):int(img_ref.shape[0]-level*height),
           max(0,int(center-width/2)):min(int(center+width/2),img_ref.shape[1])] = 1
    return output

def find_window_centroids(image, window_width, window_height, margin):
    
    window_centroids = [] # Store the (left,right) window centroid positions per level
    window = np.ones(window_width) # Create our window template that we will use for convolutions
    
    # First find the two starting positions for the left and right lane by using np.sum to get the vertical image slice
    # and then np.convolve the vertical image slice with the window template 
    
    # Sum quarter bottom of image to get slice, could use a different ratio
    l_sum = np.sum(image[int(3*image.shape[0]/4):,:int(image.shape[1]/2)], axis=0)
    l_center = np.argmax(np.convolve(window,l_sum))-window_width/2
    r_sum = np.sum(image[int(3*image.shape[0]/4):,int(image.shape[1]/2):], axis=0)
    r_center = np.argmax(np.convolve(window,r_sum))-window_width/2+int(image.shape[1]/2)
    
    # Add what we found for the first layer
    window_centroids.append((l_center,r_center))
    
    # Go through each layer looking for max pixel locations
    for level in range(1,(int)(image.shape[0]/window_height)):
        # convolve the window into the vertical slice of the image
        image_layer = np.sum(image[int(image.shape[0]-(level+1)*window_height):
                                   int(image.shape[0]-level*window_height),:], axis=0)
        conv_signal = np.convolve(window, image_layer)
        # Find the best left centroid by using past left center as a reference
        # Use window_width/2 as offset because convolution signal reference is at right side of window, not center of window
        offset = window_width/2
        l_min_index = int(max(l_center+offset-margin,0))
        l_max_index = int(min(l_center+offset+margin,image.shape[1]))
        l_center = np.argmax(conv_signal[l_min_index:l_max_index])+l_min_index-offset
        # Find the best right centroid by using past right center as a reference
        r_min_index = int(max(r_center+offset-margin,0))
        r_max_index = int(min(r_center+offset+margin,image.shape[1]))
        r_center = np.argmax(conv_signal[r_min_index:r_max_index])+r_min_index-offset
        # Add what we found for that layer
        window_centroids.append((l_center,r_center))

    return window_centroids

def visualize_convolution_method_output(binary_warped, window_centroids, window_width, window_height):    
    # If we found any window centers
    if len(window_centroids) > 0:
        # Points used to draw all the left and right windows
        l_points = np.zeros_like(binary_warped)
        r_points = np.zeros_like(binary_warped)

        # Go through each level and draw the windows 
        for level in range(0,len(window_centroids)):
            # Window_mask is a function to draw window areas
            l_mask = window_mask(window_width,window_height,binary_warped,window_centroids[level][0],level)
            r_mask = window_mask(window_width,window_height,binary_warped,window_centroids[level][1],level)
            # Add graphic points from window mask here to total pixels found 
            l_points[(l_points == 255) | ((l_mask == 1) ) ] = 255
            r_points[(r_points == 255) | ((r_mask == 1) ) ] = 255

        # Draw the results
        template = np.array(r_points+l_points,np.uint8) # add both left and right window pixels together
        zero_channel = np.zeros_like(template) # create a zero color channel
        template = np.array(cv2.merge((zero_channel,template,zero_channel)),np.uint8) # make window pixels green
        warpage= np.dstack((binary_warped, binary_warped, binary_warped))*255 # making the original road pixels 3 color channels
        output = cv2.addWeighted(warpage, 1, template, 0.3, 0.0) # overlay the orignal road image with window results

    # If no window centers found, just display orginal road image
    else:
        output = np.array(cv2.merge((binary_warped,binary_warped,binary_warped)),np.uint8)

#     # Display the final results
#     plt.imshow(output)
#     plt.title('window fitting results')
#     plt.show()
