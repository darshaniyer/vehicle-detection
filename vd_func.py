import os
import pickle
import time
import glob
import cv2
import pylab as pl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from mpl_toolkits.mplot3d import Axes3D
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from xgboost import XGBClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from skimage.feature import hog
from scipy.ndimage.measurements import label
from collections import deque
from shapely.geometry import Polygon

class Box_info():
    def __init__(self):
        self.bboxes = deque(maxlen=31)
        self.bboxes_centroid = None
        
def plot3d(ax, pixels, colors, axis_labels=list("RGB"), axis_limits=((0, 255), (0, 255), (0, 255))):
    """Plot pixels in 3D."""

    # Set axis limits
    ax.set_xlim(*axis_limits[0])
    ax.set_ylim(*axis_limits[1])
    ax.set_zlim(*axis_limits[2])

    # Set axis labels and sizes
    ax.tick_params(axis='both', which='major', labelsize=14, pad=8)
    ax.set_xlabel(axis_labels[0], fontsize=16, labelpad=16)
    ax.set_ylabel(axis_labels[1], fontsize=16, labelpad=16)
    ax.set_zlabel(axis_labels[2], fontsize=16, labelpad=16)

    # Plot pixel values with colors given in colors
    ax.scatter(
        pixels[:, :, 0].ravel(),
        pixels[:, :, 1].ravel(),
        pixels[:, :, 2].ravel(),
        c=colors.reshape((-1, 3)), edgecolors='none')

    return ax  # return Axes3D object for further manipulation

# Define a function to compute color histogram features  
def color_hist_for_plot(img, nbins=32, bins_range=(0, 256)):
    # Compute the histogram of the RGB channels separately
    hist1 = np.histogram(img[:,:,0], nbins, bins_range)
    hist2 = np.histogram(img[:,:,1], nbins, bins_range)
    hist3 = np.histogram(img[:,:,2], nbins, bins_range)
    # Generating bin centers
    bin_edges = hist1[1]
    bin_centers = (bin_edges[1:]  + bin_edges[0:len(bin_edges)-1])/2
    # Concatenate the histograms into a single feature vector
    hist_features = np.concatenate((hist1[0], hist2[0], hist3[0]))
    # Return the individual histograms, bin_centers and feature vector
    return hist1, hist2, hist3, bin_centers, hist_features
        
def convert_color(img, conv_color='RGB2YCrCb'):
    if conv_color == 'RGB2YCrCb':
        return cv2.cvtColor(img, cv2.COLOR_RGB2YCrCb)
    if conv_color == 'RGB2HSV':
        return cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    if conv_color == 'RGB2LUV':
        return cv2.cvtColor(img, cv2.COLOR_RGB2LUV)
    if conv_color == 'RGB2LAB':
        return cv2.cvtColor(img, cv2.COLOR_RGB2LAB)
    if conv_color == 'RGB2HLS':
        return cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
    if conv_color == 'RGB2YUV':
        return cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
    if conv_color == 'RGB2GRAY':
        return cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

# Define a function to compute binned color features  
def bin_spatial(img, size=(32, 32)):
    # Use cv2.resize().ravel() to create the feature vector
    bin_features = cv2.resize(img, size).ravel() 
    # Return the feature vector
    return bin_features

# Define a function to compute color histogram features  
def color_hist(img, nbins=32, bins_range=(0, 256)):
    # Compute the histogram of the color channels separately
    channel1_hist = np.histogram(img[:,:,0], bins=nbins, range=bins_range)
    channel2_hist = np.histogram(img[:,:,1], bins=nbins, range=bins_range)
    channel3_hist = np.histogram(img[:,:,2], bins=nbins, range=bins_range)
    # Concatenate the histograms into a single feature vector
    hist_features = np.concatenate((channel1_hist[0], channel2_hist[0], channel3_hist[0]))
    # Return the individual histograms, bin_centers and feature vector
    return hist_features

# Define a function to return HOG features and visualization
def get_hog_features(img, orient, pix_per_cell, cell_per_block, 
                        vis=False, feature_vec=True):
    # Call with two outputs if vis==True
    if vis == True:
        features, hog_image = hog(img, orientations=orient, pixels_per_cell=(pix_per_cell, pix_per_cell),
                                  cells_per_block=(cell_per_block, cell_per_block), block_norm= 'L2-Hys',
                                  transform_sqrt=True, 
                                  visualise=vis, feature_vector=feature_vec)
        return features, hog_image
    # Otherwise call with one output
    else:      
        features = hog(img, orientations=orient, pixels_per_cell=(pix_per_cell, pix_per_cell),
                       cells_per_block=(cell_per_block, cell_per_block), block_norm= 'L2-Hys',
                       transform_sqrt=True, 
                       visualise=vis, feature_vector=feature_vec)
        return features
    
def extract_single_image_features(img, conv_color = 'None', spatial_size=(32, 32), hist_bins=32, 
                                  orient=9, pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True):
    if conv_color != 'None':
        feature_image = convert_color(img, conv_color=conv_color)
    else:
        feature_image = np.copy(img)    
    
    img_features = []
    if spatial_feat == True:
        spatial_features = bin_spatial(feature_image, size=spatial_size)
        img_features.append(spatial_features)
    if hist_feat == True:
        # Apply color_hist()
        hist_features = color_hist(feature_image, nbins=hist_bins)
        img_features.append(hist_features)
    if hog_feat == True:
    # Call get_hog_features() with vis=False, feature_vec=True
        if hog_channel == 'ALL':
            hog_features = []
            for channel in range(feature_image.shape[2]):
                hog_features.append(get_hog_features(feature_image[:,:,channel], 
                                    orient, pix_per_cell, cell_per_block, 
                                    vis=False, feature_vec=True))
            hog_features = np.ravel(hog_features)        
        else:
            hog_features = get_hog_features(feature_image[:,:,hog_channel], orient, 
                        pix_per_cell, cell_per_block, vis=False, feature_vec=True)
        # Append the new feature vector to the features list
        img_features.append(hog_features)
    
    img_features = np.concatenate(img_features)
    
    return img_features

def extract_features(imgs, conv_color='None', spatial_size=(32, 32),
                     hist_bins=32, 
                     orient=9, pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                     spatial_feat=True, hist_feat=True, hog_feat=True):
    # Create a list to append feature vectors to
    features = []    
    # Iterate through the list of images
    for img in imgs:                
        img_features = extract_single_image_features(img, conv_color = conv_color, spatial_size=spatial_size, hist_bins=hist_bins, 
                                                     orient=orient, pix_per_cell=pix_per_cell, cell_per_block=cell_per_block, 
                                                     hog_channel=hog_channel,
                                                     spatial_feat=spatial_feat, hist_feat=hist_feat, hog_feat=hog_feat)
            
        features.append(img_features)
    # Return list of feature vectors
    return features

def classify_data(classifier, features_v_train, features_v_test, features_nv_train, features_nv_test):
    
    # Create an array stack of feature vectors
    X_train = np.vstack((features_v_train, features_nv_train)).astype(np.float64)
    y_train = np.hstack((np.ones(len(features_v_train)), np.zeros(len(features_nv_train))))
    
    X_test = np.vstack((features_v_test, features_nv_test)).astype(np.float64)
    y_test = np.hstack((np.ones(len(features_v_test)), np.zeros(len(features_nv_test))))     

    # Fit a per-column scaler only on the training data
    X_scaler = StandardScaler().fit(X_train)
    # Apply the scaler to X_train and X_test
    X_train = X_scaler.transform(X_train)
    X_test = X_scaler.transform(X_test)
    
    if (classifier == 'svm_linear'):
        clf = LinearSVC()
    elif (classifier == 'lr'):
        clf = LogisticRegression()
    elif (classifier == 'rf'):
        clf = RandomForestClassifier()
    elif (classifier == 'etc'):
        clf = ExtraTreesClassifier()
    elif (classifier == 'adaboost'):
        clf = AdaBoostClassifier()
    elif (classifier == 'xgboost'):
        clf = XGBClassifier()
    
    t=time.time()
    clf.fit(X_train, y_train)
    t2 = time.time()
    
    ttt = round(t2-t, 2)
    accuracy = round(clf.score(X_test, y_test), 4)
    
    stext = 'Test Accuracy of ' + classifier + ': '
    print(stext, accuracy)
    
    return accuracy, ttt

def get_model(car_features, notcar_features):            
   
    # Create an array stack of feature vectors
    X = np.vstack((car_features, notcar_features)).astype(np.float64)

    # Define the labels vector
    y = np.hstack((np.ones(len(car_features)), np.zeros(len(notcar_features))))
    
    # Fit a per-column scaler on the full set
    X_scaler_full = StandardScaler().fit(X)
    # Apply the scaler to X_train and X_test
    X_train_full = X_scaler_full.transform(X)
    
    # Use a linear SVC 
    clf = LinearSVC()

    clf.fit(X_train_full, y)
   
    return clf, X_scaler_full

# Here is your draw_boxes function from the previous exercise
def draw_boxes(img, bboxes, color=(0, 0, 255), thick=6):
    # Make a copy of the image
    imcopy = np.copy(img)
    # Iterate through the bounding boxes
    for bbox in bboxes:
        # Draw a rectangle given bbox coordinates
        cv2.rectangle(imcopy, bbox[0], bbox[1], color, thick) # bbox[0] = (x1,y1), bbox[1] = (x2,y2)
    # Return the image copy with boxes drawn
    return imcopy

def slide_window(img, x_start_stop=[None, None], y_start_stop=[None, None], 
                    xy_window=(64, 64), xy_overlap=(0.5, 0.5)):
    # If x and/or y start/stop positions not defined, set to image size
    if x_start_stop[0] == None:
        x_start_stop[0] = 0
    if x_start_stop[1] == None:
        x_start_stop[1] = img.shape[1]
    if y_start_stop[0] == None:
        y_start_stop[0] = 0
    if y_start_stop[1] == None:
        y_start_stop[1] = img.shape[0]
    # Compute the span of the region to be searched    
    xspan = x_start_stop[1] - x_start_stop[0]
    yspan = y_start_stop[1] - y_start_stop[0]
    # Compute the number of pixels per step in x/y
    nx_pix_per_step = np.int(xy_window[0]*(1 - xy_overlap[0])) # denominator
    ny_pix_per_step = np.int(xy_window[1]*(1 - xy_overlap[1])) # denominator
    # Compute the number of windows in x/y
    nx_buffer = np.int(xy_window[0]*(xy_overlap[0])) # numerator
    ny_buffer = np.int(xy_window[1]*(xy_overlap[1])) # numerator
    nx_windows = np.int((xspan-nx_buffer)/nx_pix_per_step) 
    ny_windows = np.int((yspan-ny_buffer)/ny_pix_per_step) 
    # Initialize a list to append window positions to
    window_list = []
    # Loop through finding x and y window positions
    # Note: you could vectorize this step, but in practice
    # you'll be considering windows one by one with your
    # classifier, so looping makes sense
    for ys in range(ny_windows):
        for xs in range(nx_windows):
            # Calculate window position
            startx = xs*nx_pix_per_step + x_start_stop[0]
            endx = startx + xy_window[0]
            starty = ys*ny_pix_per_step + y_start_stop[0]
            endy = starty + xy_window[1]
            # Append window position to list
            window_list.append(((startx, starty), (endx, endy)))
    # Return the list of windows
    return window_list

# Define a function you will pass an image 
# and the list of windows to be searched (output of slide_windows())
def search_windows(img, windows, xy_window, clf, scaler, conv_color = 'None', 
                    spatial_size=(32, 32), hist_bins=32, 
                    hist_range=(0, 256), orient=9, 
                    pix_per_cell=8, cell_per_block=2, 
                    hog_channel=0, spatial_feat=True, 
                    hist_feat=True, hog_feat=True):

    #1) Create an empty list to receive positive detection windows
    on_windows = []
    #2) Iterate over all windows in the list
    for window in windows:
        #3) Extract the test window from original image
        test_img = cv2.resize(img[window[0][1]:window[1][1], window[0][0]:window[1][0]], xy_window)      
        
        #4) Extract features for that window using single_img_features()
        features = extract_single_image_features(test_img, conv_color = conv_color, spatial_size=spatial_size, 
                                                 hist_bins=hist_bins, orient=orient, pix_per_cell=pix_per_cell, 
                                                 cell_per_block=cell_per_block, hog_channel=hog_channel,
                                                 spatial_feat=spatial_feat, hist_feat=hist_feat, hog_feat=hog_feat)
        #5) Scale extracted features to be fed to classifier
        test_features = scaler.transform(np.array(features).reshape(1, -1))
        #6) Predict using your classifier
        prediction = clf.predict(test_features)
        #7) If positive (prediction == 1) then save the window
        if prediction == 1:
            on_windows.append(window)
    #8) Return windows for positive detections
    return on_windows

def search_windows2(img, windows, xy_window, selector, clf, scaler, conv_color = 'None', 
                    spatial_size=(32, 32), hist_bins=32, 
                    hist_range=(0, 256), orient=9, 
                    pix_per_cell=8, cell_per_block=2, 
                    hog_channel=0, spatial_feat=True, 
                    hist_feat=True, hog_feat=True):

    #1) Create an empty list to receive positive detection windows
    on_windows = []
    #2) Iterate over all windows in the list
    for window in windows:
        #3) Extract the test window from original image
        test_img = cv2.resize(img[window[0][1]:window[1][1], window[0][0]:window[1][0]], xy_window)      
        
        #4) Extract features for that window using single_img_features()
        features = extract_single_image_features(test_img, conv_color = conv_color, spatial_size=spatial_size, 
                                                 hist_bins=hist_bins, orient=orient, pix_per_cell=pix_per_cell, 
                                                 cell_per_block=cell_per_block, hog_channel=hog_channel,
                                                 spatial_feat=spatial_feat, hist_feat=hist_feat, hog_feat=hog_feat)
        #5) Scale extracted features to be fed to classifier
        test_features = scaler.transform(np.array(features).reshape(1, -1))
        #6) Select features
        test_features_sel = selector.transform(test_features)        
        #7) Predict using your classifier
        prediction = clf.predict(test_features_sel)
        #7) If positive (prediction == 1) then save the window
        if prediction == 1:
            on_windows.append(window)
    #8) Return windows for positive detections
    return on_windows

def find_cars_hybrid(img, conv_color, xstart, xstop, ystart, ystop, scale, window, scaler, svm_model, xgb_model,
                     orient, pix_per_cell, cell_per_block, cells_per_step, spatial_size, hist_bins, hog_channel):    
  
    draw_img = np.copy(img)
    hot_windows = []    
  
    img_tosearch = img[ystart:ystop,:,:]
    ctrans_tosearch = convert_color(img_tosearch, conv_color=conv_color)
    if scale != 1:
        imshape = ctrans_tosearch.shape
        ctrans_tosearch = cv2.resize(ctrans_tosearch, (np.int(imshape[1]/scale), np.int(imshape[0]/scale)))
        
    ch1 = ctrans_tosearch[:,:,0]
    ch2 = ctrans_tosearch[:,:,1]
    ch3 = ctrans_tosearch[:,:,2]

    # Define blocks and steps as above
    nxblocks = (ch1.shape[1] // pix_per_cell) - cell_per_block + 1
    nyblocks = (ch1.shape[0] // pix_per_cell) - cell_per_block + 1 
    nfeat_per_block = orient*cell_per_block**2
    
    nblocks_per_window = (window // pix_per_cell) - cell_per_block + 1
    cells_per_step = cells_per_step  # Instead of overlap, define how many cells to step
    nxsteps = (nxblocks - nblocks_per_window) // cells_per_step + 1
    nysteps = (nyblocks - nblocks_per_window) // cells_per_step + 1
    
    # Compute individual channel HOG features for the entire image
    hog1 = get_hog_features(ch1, orient, pix_per_cell, cell_per_block, feature_vec=False)   

    for xb in range(nxsteps):
        for yb in range(nysteps):
            ypos = yb*cells_per_step
            xpos = xb*cells_per_step
            
            # Extract HOG for this patch
            hog_features = hog1[ypos:ypos+nblocks_per_window, xpos:xpos+nblocks_per_window].ravel() 

            xleft = xpos*pix_per_cell
            ytop = ypos*pix_per_cell

            # Extract the image patch
            subimg = cv2.resize(ctrans_tosearch[ytop:ytop+window, xleft:xleft+window], (64,64))
          
            # Get color features
            spatial_features = bin_spatial(subimg, size=spatial_size)
            hist_features = color_hist(subimg, nbins=hist_bins)

            # Scale features and make a prediction
            test_features_s = scaler.transform(np.hstack((spatial_features, hist_features, hog_features)).reshape(1, -1))  
            svm_test_prediction = svm_model.predict(test_features_s)
            
            if (svm_test_prediction == 1):
                xgb_test_prediction = xgb_model.predict(test_features_s)
                if (xgb_test_prediction == 1):                
                    xbox_left = np.int(xleft*scale)
                    ytop_draw = np.int(ytop*scale)
                    win_draw = np.int(window*scale)
                    x1 = xbox_left
                    y1 = ytop_draw+ystart
                    x2 = xbox_left+win_draw
                    y2 = ytop_draw+win_draw+ystart
    #                 cv2.rectangle(draw_img,(x1,y1),(x2,y2),(0,0,255),6)
                    if (x1 > xstart+200):
                        win = ((x1,y1), (x2,y2))
                        hot_windows.append(win)  
        
    return hot_windows

def find_cars_sel(img, conv_color, xstart, xstop, ystart, ystop, scale, window, selector, clf, X_scaler, 
                  orient, pix_per_cell, cell_per_block, cells_per_step, spatial_size, hist_bins, hog_channel):    
  
    draw_img = np.copy(img)
    hot_windows = []    
  
    img_tosearch = img[ystart:ystop,:,:]
    ctrans_tosearch = convert_color(img_tosearch, conv_color=conv_color)
    if scale != 1:
        imshape = ctrans_tosearch.shape
        ctrans_tosearch = cv2.resize(ctrans_tosearch, (np.int(imshape[1]/scale), np.int(imshape[0]/scale)))
        
    ch1 = ctrans_tosearch[:,:,0]
    ch2 = ctrans_tosearch[:,:,1]
    ch3 = ctrans_tosearch[:,:,2]

    # Define blocks and steps as above
    nxblocks = (ch1.shape[1] // pix_per_cell) - cell_per_block + 1
    nyblocks = (ch1.shape[0] // pix_per_cell) - cell_per_block + 1 
    nfeat_per_block = orient*cell_per_block**2
    
    nblocks_per_window = (window // pix_per_cell) - cell_per_block + 1
    cells_per_step = cells_per_step  # Instead of overlap, define how many cells to step
    nxsteps = (nxblocks - nblocks_per_window) // cells_per_step + 1
    nysteps = (nyblocks - nblocks_per_window) // cells_per_step + 1
    
    # Compute individual channel HOG features for the entire image
    hog1 = get_hog_features(ch1, orient, pix_per_cell, cell_per_block, feature_vec=False)
    hog2 = get_hog_features(ch2, orient, pix_per_cell, cell_per_block, feature_vec=False)
    hog3 = get_hog_features(ch3, orient, pix_per_cell, cell_per_block, feature_vec=False)    

    for xb in range(nxsteps):
        for yb in range(nysteps):
            ypos = yb*cells_per_step
            xpos = xb*cells_per_step
            # Extract HOG for this patch
            hog_feat1 = hog1[ypos:ypos+nblocks_per_window, xpos:xpos+nblocks_per_window].ravel() 
            hog_feat2 = hog2[ypos:ypos+nblocks_per_window, xpos:xpos+nblocks_per_window].ravel() 
            hog_feat3 = hog3[ypos:ypos+nblocks_per_window, xpos:xpos+nblocks_per_window].ravel() 
            if (hog_channel == 'ALL'):
                hog_features = np.hstack((hog_feat1, hog_feat2, hog_feat3))
            elif (hog_channel == 0):
                hog_features = hog_feat1
            elif (hog_channel == 1):
                hog_features = hog_feat2
            elif (hog_channel == 2):
                hog_features = hog_feat3               

            xleft = xpos*pix_per_cell
            ytop = ypos*pix_per_cell

            # Extract the image patch
            subimg = cv2.resize(ctrans_tosearch[ytop:ytop+window, xleft:xleft+window], (64,64))
          
            # Get color features
            spatial_features = bin_spatial(subimg, size=spatial_size)
            hist_features = color_hist(subimg, nbins=hist_bins)

            # Scale features and make a prediction
            test_features = X_scaler.transform(np.hstack((spatial_features, hist_features, hog_features)).reshape(1, -1))  
            test_features_sel = selector.transform(test_features)    
            test_prediction = clf.predict(test_features_sel)
            
            if (test_prediction == 1):
                xbox_left = np.int(xleft*scale)
                ytop_draw = np.int(ytop*scale)
                win_draw = np.int(window*scale)
                x1 = xbox_left
                y1 = ytop_draw+ystart
                x2 = xbox_left+win_draw
                y2 = ytop_draw+win_draw+ystart
#                 cv2.rectangle(draw_img,(x1,y1),(x2,y2),(0,0,255),6)
                if (x1 > xstart+200):
                    win = ((x1,y1), (x2,y2))
                    hot_windows.append(win)  
        
    return hot_windows

def find_cars(img, conv_color, xstart, xstop, ystart, ystop, scale, window, svc, X_scaler, 
              orient, pix_per_cell, cell_per_block, cells_per_step, spatial_size, hist_bins, hog_channel):    
  
    draw_img = np.copy(img)
    hot_windows = []    
  
    img_tosearch = img[ystart:ystop,:,:]
    ctrans_tosearch = convert_color(img_tosearch, conv_color=conv_color)
    if scale != 1:
        imshape = ctrans_tosearch.shape
        ctrans_tosearch = cv2.resize(ctrans_tosearch, (np.int(imshape[1]/scale), np.int(imshape[0]/scale)))
        
    ch1 = ctrans_tosearch[:,:,0]
    ch2 = ctrans_tosearch[:,:,1]
    ch3 = ctrans_tosearch[:,:,2]

    # Define blocks and steps as above
    nxblocks = (ch1.shape[1] // pix_per_cell) - cell_per_block + 1
    nyblocks = (ch1.shape[0] // pix_per_cell) - cell_per_block + 1 
    nfeat_per_block = orient*cell_per_block**2
    
    # 64 was the original sampling rate, with 8 cells and 8 pix per cell
    nblocks_per_window = (window // pix_per_cell) - cell_per_block + 1
    cells_per_step = cells_per_step  # Instead of overlap, define how many cells to step
    nxsteps = (nxblocks - nblocks_per_window) // cells_per_step + 1
    nysteps = (nyblocks - nblocks_per_window) // cells_per_step + 1
    
    # Compute individual channel HOG features for the entire image
    hog1 = get_hog_features(ch1, orient, pix_per_cell, cell_per_block, feature_vec=False)
    hog2 = get_hog_features(ch2, orient, pix_per_cell, cell_per_block, feature_vec=False)
    hog3 = get_hog_features(ch3, orient, pix_per_cell, cell_per_block, feature_vec=False)    

    for xb in range(nxsteps):
        for yb in range(nysteps):
            ypos = yb*cells_per_step
            xpos = xb*cells_per_step
            # Extract HOG for this patch
            if (hog_channel == 'ALL'):
                hog_feat1 = hog1[ypos:ypos+nblocks_per_window, xpos:xpos+nblocks_per_window].ravel() 
                hog_feat2 = hog2[ypos:ypos+nblocks_per_window, xpos:xpos+nblocks_per_window].ravel() 
                hog_feat3 = hog3[ypos:ypos+nblocks_per_window, xpos:xpos+nblocks_per_window].ravel() 
                hog_features = np.hstack((hog_feat1, hog_feat2, hog_feat3))
            elif (hog_channel == 0):
                hog_features = hog_feat1
            elif (hog_channel == 1):
                hog_features = hog_feat2
            elif (hog_channel == 2):
                hog_features = hog_feat3  

            xleft = xpos*pix_per_cell
            ytop = ypos*pix_per_cell

            # Extract the image patch
            subimg = cv2.resize(ctrans_tosearch[ytop:ytop+window, xleft:xleft+window], (64,64))
          
            # Get color features
            spatial_features = bin_spatial(subimg, size=spatial_size)
            hist_features = color_hist(subimg, nbins=hist_bins)

            # Scale features and make a prediction
            test_features = X_scaler.transform(np.hstack((spatial_features, hist_features, hog_features)).reshape(1, -1))  
            test_prediction = svc.predict(test_features)
            
            if test_prediction == 1:
                xbox_left = np.int(xleft*scale)
                ytop_draw = np.int(ytop*scale)
                win_draw = np.int(window*scale)
                x1 = xbox_left
                y1 = ytop_draw+ystart
                x2 = xbox_left+win_draw
                y2 = ytop_draw+win_draw+ystart
#                 cv2.rectangle(draw_img,(x1,y1),(x2,y2),(0,0,255),6)
                if (x1 > xstart):
                    win = ((x1,y1), (x2,y2))
                    hot_windows.append(win)  
        
    return hot_windows
   
def add_heat(heatmap, bbox_list):
    # Iterate through list of bboxes
    for box in bbox_list:
        # Add += 1 for all pixels inside each bbox
        # Assuming each "box" takes the form ((x1, y1), (x2, y2))
        heatmap[box[0][1]:box[1][1], box[0][0]:box[1][0]] += 1

    # Return updated heatmap
    return heatmap# Iterate through list of bboxes
    
def apply_threshold(heatmap, threshold):
    # Zero out pixels below the threshold
    heatmap[heatmap <= threshold] = 0
    # Return thresholded map
    return heatmap

def draw_labeled_bboxes(img, labels):    
    draw_image = np.copy(img)
    # Iterate through all detected cars
    bboxes = []
    for car_number in range(1, labels[1]+1):
        # Find pixels with each car_number label value
        nonzero = (labels[0] == car_number).nonzero()
        # Identify x and y values of those pixels
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        # Define a bounding box based on min/max x and y
        bbox = ((np.min(nonzerox), np.min(nonzeroy)), (np.max(nonzerox), np.max(nonzeroy)))
        # Draw the box on the image
        cv2.rectangle(draw_image, bbox[0], bbox[1], (0,0,255), 6)
        bboxes.append((bbox[0], bbox[1]))
#         x1 = bbox[0][1]; y1 = bbox[0][0]; x2 = bbox[1][1]; y2 = bbox[1][0];
#         if (x2-x1 > 100 and y2-y1 > 75):
#             cv2.rectangle(draw_image, bbox[0], bbox[1], (0,0,255), 6)
#             bboxes.append((bbox[0], bbox[1]))
        
    # Return the image
    return draw_image, bboxes

def get_bounding_boxes(labels):
    # Iterate through all detected cars
    bboxes = []
    for car_number in range(1, labels[1]+1):
        # Find pixels with each car_number label value
        nonzero = (labels[0] == car_number).nonzero()
        # Identify x and y values of those pixels
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        # Define a bounding box based on min/max x and y
        bbox = ((np.min(nonzerox), np.min(nonzeroy)), (np.max(nonzerox), np.max(nonzeroy)))        
        bboxes.append((bbox[0], bbox[1]))
#         x1 = bbox[0][1]; y1 = bbox[0][0]; x2 = bbox[1][1]; y2 = bbox[1][0];
#         if (x2-x1 > 100 and y2-y1 > 75):
#             cv2.rectangle(draw_image, bbox[0], bbox[1], (0,0,255), 6)
#             bboxes.append((bbox[0], bbox[1]))      

    # Return the tuples of bounding boxes
    return bboxes

def depth(t):
    try:
        return 1+max(map(depth,t))
    except:
        return 0
    
def flatten_list(bboxes):
    
    bboxes_flattened = []
    for x in bboxes:
        if (depth(x) > 2):
            for y in x:
                bboxes_flattened.append(y)
        else:
            bboxes_flattened.append(x) 
            
    return bboxes_flattened   

def calc_heatmap(bboxes_flattened, img):
    
    heat = np.zeros_like(img[:,:,0]).astype(np.float)

    # Add heat to each box in box list
    heat = add_heat(heat, bboxes_flattened)

    # Apply threshold to help remove false positives
    heat = apply_threshold(heat, np.mean(heat))

    # Visualize the heatmap when displaying    
    heatmap = np.clip(heat, 0, 255)
    
    return heatmap
    
def calc_centroid_box(bboxes, img):    

    bboxes_flattened = flatten_list(bboxes)   
    
    heatmap = calc_heatmap(bboxes_flattened, img)   

    # Find final boxes from heatmap using label function
    labels = label(heatmap)
    bboxes_centroid = get_bounding_boxes(labels)
    
    heatmap_robust = calc_heatmap(bboxes_centroid, img)    
    
    return bboxes_centroid, heatmap_robust

def calc_centroid_box_with_filter(bboxes, img, poly, thresh_lo, thresh_hi, shrink_rate, dire):    

    bboxes_flattened = flatten_list(bboxes)   
    
    heatmap = calc_heatmap(bboxes_flattened, img)   

    # Find final boxes from heatmap using label function
    labels = label(heatmap)
    bboxes_centroid = get_bounding_boxes(labels)
    
    bboxes_centroid_fil = overlap_and_filter(bboxes_centroid, img, poly, thresh_lo, thresh_hi, shrink_rate, dire) 
    
    heatmap_robust = calc_heatmap(bboxes_centroid_fil, img)    
    
    return bboxes_centroid_fil, heatmap_robust

def overlap_and_filter(bboxes_centroid, img, poly, thresh_lo, thresh_hi, shrink_rate, dire):   
    
    y_max = img.shape[0]
    A = poly[0]
    B = poly[1]
    C = poly[2]
    D = poly[3]
    poly_1 = np.float32(((A[0], y_max-A[1]), (B[0], y_max-B[1]), (C[0], y_max-C[1]), (D[0], y_max-D[1])))
#     print(poly_1)
    p1 = Polygon(poly_1)

    boxes_retain = []
    for box in bboxes_centroid:
        y1 = y_max-box[0][1]
        y2 = y_max-box[1][1]
        x1 = box[0][0]
        x2 = box[1][0]

        rect = np.float32([(x1,y1), (x1,y2), (x2,y2), (x2,y1)])
#         print(rect)

        p2 = Polygon(rect)

        intersect_area = p1.intersection(p2).area
        
        po = 0.0
        if (p2.area > 0):
            po = intersect_area/p2.area # percent overlap          
            
#         print(po)
        if (po <= thresh_hi): 
#             print(box)
            if (po > thresh_lo): 
                box = shrink(box, shrink_rate, dire)
#             print(box)
            boxes_retain.append(box)
            
#     print(boxes_retain)
    
    return boxes_retain

def shrink(box, shrink_rate, dire='right'):
    
    (x1,y1), (x2,y2) = box
    
    if (dire == 'right'):
        x1_n = np.int32(x1 + (x2-x1)*shrink_rate/2)
        x2_n = np.int32(x2 - (x2-x1)*shrink_rate/8)
        y1_n = np.int32(y1 + (y2-y1)*shrink_rate/4)
        y2_n = np.int32(y2 - (y2-y1)*shrink_rate/4)  
    elif (dire == 'left'):    
        x1_n = np.int32(x1 + (x2-x1)*shrink_rate/8)
        x2_n = np.int32(x2 - (x2-x1)*shrink_rate/2)
        y1_n = np.int32(y1 + (y2-y1)*shrink_rate/4)
        y2_n = np.int32(y2 - (y2-y1)*shrink_rate/4)         
    elif (dire == 'up'):
        y1_n = np.int32(y1 + (y2-y1)*shrink_rate/2)
        y2_n = np.int32(y2 - (y2-y1)*shrink_rate/8)    
        x1_n = np.int32(x1 + (x2-x1)*shrink_rate/4)
        x2_n = np.int32(x2 - (x2-x1)*shrink_rate/4)
    elif (dire == 'down'):
        y1_n = np.int32(y1 + (y2-y1)*shrink_rate/8)
        y2_n = np.int32(y2 - (y2-y1)*shrink_rate/2) 
        x1_n = np.int32(x1 + (x2-x1)*shrink_rate/4)
        x2_n = np.int32(x2 - (x2-x1)*shrink_rate/4)
    
    box_shrink = ((x1_n,y1_n), (x2_n,y2_n)) 
    
    return box_shrink