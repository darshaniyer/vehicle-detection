## Vehicle detection

When we drive, we use our eyes to decide where to go.  We look at road signs, lanes, and other objects such as vehicles and pedestrians. Previously, we developed a computer vision algorithm based on deep learning to detect 43 different road signs. We also developed algorithms based on classical computer vision to detect straight and solid lane lines under uniform lightning conditions, as well as lane lines that could be highly curved and broken with variable visibility brought about by highly varying lightning conditions caused by shadows, different pavement colors etc. The current project is primarily focused on developing a vehicle detection algorithm based on a combination of classical computer vision for data preprocessing and feature extraction, and classical machine learning for recognizing and locating vehicles from non-vehicles based on the features in each image, and finally track these detections from frame to frame in a video stream. Though the primary focus is a pipeline based on classical computer vision and machine learning, we have also processed the videos using deep learning based object detection algorithms such as SSD and YOLO.

## Overall steps
The algorithm consists of the following steps:

* Organize data from vehicles and non-vehicles
* Generate training and test sets
* Apply color transform
* Extract features based on binned color features, histograms of color, and histogram of oriented gradients (HOG)
* Normalize the features
* Perform feature selection
* Train a classifier using training set and test its performance on the test set
* Build a final model using the entire dataset
* Implement a sliding-window technique and use the trained classifier to search for vehicles in images
* Estimate a bounding box for vehicles detected
* Run the pipeline on a video stream and create a heat map of recurring detections frame by frame to reject outliers and follow   detected vehicles.

The method works on each image from the video clip. In the end, the processed images are framed together to generate a new 
video clip with the vehicles localized with bounding boxes along with detected lanes superimposed on each frame of the video along with their respective curvature values and the position of the ego vehicle with respect to the center. 

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Vehicle-Detection).

## Dependencies

This project requires:

* [CarND Term1 Starter Kit](https://github.com/udacity/CarND-Term1-Starter-Kit)

The lab environment can be created with CarND Term1 Starter Kit. Click [here](https://github.com/udacity/CarND-Term1-Starter-Kit/blob/master/README.md) for the details.

## Detailed Writeup

Detailed report can be found in [_VehicleDetection_writeup.md_](VehicleDetection_writeup.md).

## Solution video

![Classical method](./videos/project_video_out_smooth.mp4)

![Deep learning based method](./videos/project_video_out_smooth_DL.mp4)

## Acknowledgments

The beauty is in the details for which a whitebox approach based on classical methods is crucial to understanding the complexity and the nuts and bolts of the object detection problem. This project gave us an exposure to cutting edge classical machine learning algorithm such as XGBoost, and its hybrid version with support vector classifier as the first layer. XGBoost provided robustness, and support vector classifier provided speed. I would like to thank Udacity for giving me this opportunity to work on an awesome project. 

